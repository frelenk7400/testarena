using Player;
using Spawners;
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private PlayerView playerCanvasPrefab;
    [SerializeField] private PlayerPresenter playerPrefab;
    [SerializeField] private Transform playerStartPlace;
    public override void InstallBindings()
    {
        Time.timeScale = 1;
        Container.Bind<BulletSpawner>().AsSingle().NonLazy();
        Container.Bind<SpawnersHandler>().AsSingle().NonLazy();
        InitPlayer();
    }

    private void InitPlayer()
    {
        PlayerPresenter playerPresenter =
            Container.InstantiatePrefabForComponent<PlayerPresenter>(playerPrefab, playerStartPlace);
        Container.BindInstance(playerPresenter);
        
        PlayerView playerView = Container.InstantiatePrefabForComponent<PlayerView>(playerCanvasPrefab);
        Container.BindInstance(playerView);

        PlayerModel playerModel = Container.Instantiate<PlayerModel>();

        Container.BindInstance(playerModel);
        
        playerPresenter.Init(playerModel);
        playerView.Init(playerPresenter);
    }
    
}
