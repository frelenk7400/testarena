using System;
using System.Collections.Generic;
using System.Linq;
using Enemies;
using Misc.Bullet;
using Player;
using StaticVariables;
using UnityEngine;
using UnityEngine.Serialization;
using Zenject;

namespace Infrastructure.Installers
{
    [CreateAssetMenu(menuName = "Arena/Game Settings")]
    public class GameSettingsInstaller : ScriptableObjectInstaller<GameSettingsInstaller>
    {
        public PlayerSettings Player;
        public EnemySettings Enemies;
        public BulletSettings Bullets;
        public override void InstallBindings()
        {
            Container.BindInstance(Player);
            Container.BindInstance(Bullets);
            Container.BindInstance(Enemies);
        }
    
        [Serializable]
        public class PlayerSettings
        {
            public PlayerModel.Settings ModelSettings;
        }
    
        [Serializable]
        public class EnemySettings
        {
            public List<EnemyData> EnemiesSettings;

            public EnemyData GetEnemyData(EnemyTypes enemyType) =>
                EnemiesSettings.FirstOrDefault(x => x.EnemyType == enemyType);
        
            [Serializable]
            public class EnemyData
            {
                public EnemyTypes EnemyType;
                public Enemy EnemyPrefab;
                public int MaxHP;
                public int StartHP;
                public float Speed;
                public float AngularSpeed;
                public int Damage;
                public int UltaPointsForDefeat;
            }
        }
    
        [Serializable]
        public class BulletSettings
        {
            public PlayerBullet PlayerBulletPrefab;
            public Bullet.Settings PlayerBulletSettings;
            public EnemyBullet EnemyBulletPrefab;
            public Bullet.Settings EnemyBulletSettings;
        }
    }
}
