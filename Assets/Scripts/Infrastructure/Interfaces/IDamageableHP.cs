namespace Infrastructure.Interfaces
{
    public interface IDamageableHP
    {
        public int CurrentHp { get; }
        public void SetDamage(int damage, bool killedByReflect = false);
    }
}
