﻿using System;
using UnityEngine;

namespace Infrastructure.Interfaces
{
    public interface IReleasable
    {
        public event Action<GameObject> OnItemRelease;
    }
}