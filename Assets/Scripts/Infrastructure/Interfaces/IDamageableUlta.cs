namespace Infrastructure.Interfaces
{
    public interface IDamageableUlta
    {
        public void ChangeUltaValue(int ultaValue);
    }
}
