﻿using System;
using UnityEngine;

namespace Infrastructure.Services.Input
{
    public abstract class InputService: IInputService
    {
        protected const string HorizontalMovement = "HorizontalMovement";
        protected const string VerticalMovement = "VerticalMovement";
        protected const string HorizontalRotation = "HorizontalRotation";
        protected const string VerticalRotation = "VerticalRotation";
        protected const string AttackButton = "Attack";
        protected const string UltiButton = "Ulti";
        
        public abstract Vector2 MovementAxis { get; }
        public abstract Vector2 RotationAxis { get; }

        public bool IsAttackButtonUp() =>
            SimpleInput.GetButton(AttackButton);

        public bool IsUltiButtonUp() =>
            SimpleInput.GetButton(UltiButton);
    }
}