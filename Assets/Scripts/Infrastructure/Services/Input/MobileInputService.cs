﻿using UnityEngine;

namespace Infrastructure.Services.Input
{
    public class MobileInputService: InputService
    {
        public override Vector2 MovementAxis =>
            new (SimpleInput.GetAxis(HorizontalMovement), SimpleInput.GetAxis(VerticalMovement));
        public override Vector2 RotationAxis =>
            new (SimpleInput.GetAxis(HorizontalRotation), SimpleInput.GetAxis(VerticalRotation));
    }
}