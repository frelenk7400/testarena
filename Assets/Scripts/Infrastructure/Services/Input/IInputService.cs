using UnityEngine;

namespace Infrastructure.Services.Input
{
    public interface IInputService
    {
       public Vector2 MovementAxis { get; }
       public Vector2 RotationAxis { get; }

       bool IsAttackButtonUp();
       bool IsUltiButtonUp();
    }
}
