using Misc.Bullet;
using UnityEngine;

namespace Infrastructure.Factories
{
    public class BulletFactory<T> : GameObjectsCreator<T> where T : Bullet
    {
        public BulletFactory(T prefab) : base(prefab)
        {
        }
        
    }
}
