using Enemies;
using Infrastructure.Factories;

public class EnemyFactory : GameObjectsCreator<Enemy>
{
    public EnemyFactory(Enemy prefab) : base(prefab)
    {
    }
}
