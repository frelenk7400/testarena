using Infrastructure.Interfaces;
using Misc;
using UnityEngine;
using UnityEngine.Pool;
using Object = UnityEngine.Object;


namespace Infrastructure.Factories
{
    public abstract class GameObjectsCreator<TPrefab> where TPrefab : MonoBehaviour, IReleasable
    {
        private readonly TPrefab _prefab;

        private Transform _parent;

        private ObjectPool<TPrefab> pool { get; }

        protected GameObjectsCreator(TPrefab prefab)
        {
            pool = new ObjectPool<TPrefab>(FactoryMethod, Init, OnRelease);
            _prefab = prefab;

            GameObject blankGameObject = new GameObject();
            blankGameObject.name = prefab.name;
            _parent = blankGameObject.transform;
        }
        
        public TPrefab FactoryMethod()
        {
            TPrefab gameObject = Object.Instantiate(_prefab, _parent);
            
            return gameObject.GetComponent<TPrefab>();
        }

        protected void Init(TPrefab gameObject)
        {
            gameObject.gameObject.SetActive(true);
            gameObject.OnItemRelease += ReleaseObject;
        }

        protected void OnRelease(TPrefab gameObject)
        {
            gameObject.OnItemRelease -= ReleaseObject;
            gameObject.gameObject.SetActive(false);
        }

        private void ReleaseObject(GameObject gameObject) =>
            pool.Release(gameObject.GetComponent<TPrefab>());

        public TPrefab GetObject() =>
            pool.Get();
    }
}
