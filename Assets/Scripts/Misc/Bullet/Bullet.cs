using System;
using Infrastructure.Interfaces;
using UnityEngine;

namespace Misc.Bullet
{
    public abstract class Bullet : MonoBehaviour, IReleasable
    {
        protected float _speed;
        protected int _damage;
        protected float _angularSpeed;
        public event Action<GameObject> OnItemRelease;

        protected Transform _startTransform;
        private float _timeElapsed;
        private float _timeToDispose;
        private bool isInit;

        public void Init(Transform starTransform)
        {
            _startTransform = starTransform;
            transform.position = _startTransform.position;
            transform.rotation = _startTransform.rotation;
            _timeElapsed = 0;
            
            isInit = true;
        }

        public void SetSettings(Settings settings)
        {
            _speed = settings.speed;
            _damage = settings.damage;
            _angularSpeed = settings.angularSpeed;
            _timeToDispose = settings.timeToDispose;
        }
        public void Update()
        {
            if (isInit)
            {
                Move();

                if (_timeElapsed > _timeToDispose)
                {
                    Release();
                }
                
                _timeElapsed += Time.deltaTime;
            }
        }

        public abstract void Move();

        protected void Release() =>
            OnItemRelease?.Invoke(gameObject);
        
        public abstract void OnTriggerEnter(Collider other);
        
        [Serializable]
        public class Settings
        {
            public float speed;
            public float angularSpeed;
            public int damage;
            public float timeToDispose;
        }
    }
}
