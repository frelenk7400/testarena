﻿using System;
using Infrastructure.Interfaces;
using UnityEngine;

namespace Misc.Bullet
{
    public class EnemyBullet : Bullet
    {
        private Transform _targetToMove;
        private bool isFlyingToPoint;
        public void Init(Transform startTransform, Transform targetToMove,ref Action OnPlayerTeleportated)
        {
            base.Init(startTransform);
            _targetToMove = targetToMove;
            isFlyingToPoint = false;
            
            OnPlayerTeleportated += ChangeMoveToPoint;
        }

        public override void Move()
        {
            if (!isFlyingToPoint)
            {
                Vector3 direction = (_targetToMove.position - transform.position).normalized;
            
                Quaternion targetRotation = Quaternion.LookRotation(direction);
            
                transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, _angularSpeed * Time.deltaTime);
            
                transform.Translate(transform.forward * _speed * Time.deltaTime, Space.World);
            }
            else
            {
                transform.Translate(transform.forward * _speed * Time.deltaTime, Space.World);
            }
        }

        public void ChangeMoveToPoint()
        {
            isFlyingToPoint = true;
        }
           

        public override void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IDamageableUlta ulta))
            {
                ulta.ChangeUltaValue(-_damage);
            }
           
            Release();
        }
    }
}