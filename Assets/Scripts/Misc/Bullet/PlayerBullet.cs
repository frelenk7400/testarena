﻿using System;
using Infrastructure.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Misc.Bullet
{
    public class PlayerBullet : Bullet
    {
        private bool isReflected;
        private Transform _targetToReflect;
        private Action<PlayerBullet, Vector3> OnEnemyKill;
        public void Init(Transform starTransform, Action<PlayerBullet, Vector3> OnEnemyKill)
        {
            isReflected = false;
            this.OnEnemyKill = OnEnemyKill;
            base.Init(starTransform);
        }

        public override void Move()
        {
            if (!isReflected)
            {
                transform.Translate(transform.forward * _speed * Time.deltaTime, Space.World);
            }
            else
            {
                Vector3 directionToMove = _targetToReflect.position - transform.position;
                directionToMove.Normalize();
                transform.position += directionToMove * _speed * Time.deltaTime;
            }
        }

        public void SetTargetToReflect(Transform targetToReflect, int playerHp)
        {
            int chanceToReflect = Random.Range(1, 100);

            if (chanceToReflect > playerHp)
            {
                _targetToReflect = targetToReflect;
                isReflected = true;
            }
            else
            {
                Release();
            }
        }
        
        public override void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IDamageableHP health))
            {
                health.SetDamage(_damage);

                if (health.CurrentHp == 0)
                {
                    OnEnemyKill?.Invoke(this, other.transform.position);
                }
                else
                {
                    Release();
                }
            }
            else
            {
                Release();
            }
        }

       
    }
}