using System;
using System.Collections.Generic;
using Enemies;
using UnityEngine;

namespace Spawners
{
   public class SpawnersHandler
   {
      public event Action<int, int> OnEnemyDie;

      public int EnemiesKilled { get; private set; }
      
      private List<Enemy> allActiveEnemies = new();

      public void AddEnemy(Enemy enemy)
      {
         allActiveEnemies.Add(enemy);
         enemy.OnEnemyDie += GetRewardForEnemy;
      }


      public void RemoveEnemy(Enemy enemy)
      {
         allActiveEnemies.Remove(enemy);
         enemy.OnEnemyDie -= GetRewardForEnemy;
      }

      public void GetRewardForEnemy(int ultaPoints, int hpPoints)
      {
         Debug.Log("get");
         OnEnemyDie?.Invoke(ultaPoints, hpPoints);
         EnemiesKilled++;
      }
         

      public void KillAllEnemies()
      {
         for (int i = 0; i < allActiveEnemies.Count; i++)
         {
            allActiveEnemies[i].KillWithoutReward();
            i--;
         }
      }

      public Transform GetNearestEnemy(Vector3 searchPosition)
      {
         Transform closestTransform = null;
         
         float closestDistance = Mathf.Infinity;
         foreach (Enemy enemy in allActiveEnemies)
         {
            float distance = Vector3.Distance(enemy.transform.position, searchPosition);

            if (distance < closestDistance)
            {
               closestDistance = distance;
               closestTransform = enemy.transform;
            }
         }

         return closestTransform;
      }

      public bool IsPositionAwayFromEnemies(Vector3 startPosition, float distanceAway)
      {
         foreach (Enemy enemy in allActiveEnemies)
         {
            float distance = Vector3.Distance(enemy.transform.position, startPosition);

            if (distance < distanceAway)
            {
               return false;
            }
         }

         return true;
      }
   }
}
