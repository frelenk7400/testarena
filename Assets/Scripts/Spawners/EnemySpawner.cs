using System;
using System.Collections;
using System.Collections.Generic;
using Enemies;
using Infrastructure.Factories;
using Infrastructure.Installers;
using Player;
using StaticVariables;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Random = UnityEngine.Random;

namespace Spawners
{
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private float spawnRadius;
        private Dictionary<EnemyTypes, GameObjectsCreator<Enemy>> _enemyFactories;
        private GameSettingsInstaller.EnemySettings _enemySettings;
        private SpawnersHandler _spawnersHandler;
        

        private Transform _playerTransform;
    
        private float _spawnTimer = 5f;
        private float _minSpawnTime = 2f;

        private int _blueEnemyCount = 1;
        private int _redEnemyCount = 4;

        [Inject]
        public void Construct(PlayerPresenter playerPresenter, GameSettingsInstaller.EnemySettings enemySettings,
            SpawnersHandler spawnersHandler)
        {
            _enemySettings = enemySettings;
            _playerTransform = playerPresenter.transform;
            _spawnersHandler = spawnersHandler;
        }
    
        private void Start()
        {
            CreateFactories();
             StartCoroutine(SpawnEnemies());
            
        }

        private void CreateFactories()
        {
            _enemyFactories = new();

            foreach (EnemyTypes enemyType in Enum.GetValues(typeof(EnemyTypes)))
            {
                Enemy enemyPrefab = _enemySettings.GetEnemyData(enemyType).EnemyPrefab;
                _enemyFactories.Add(enemyType,new EnemyFactory(enemyPrefab));
            }
        }
    
        private IEnumerator SpawnEnemies()
        {
            while (true)
            {
                for (int i = 0; i < _blueEnemyCount; i++)
                {
                     SpawnEnemy(EnemyTypes.Blue);
                }
                
                for (int i = 0; i < _redEnemyCount; i++)
                {
                    SpawnEnemy(EnemyTypes.Red);
                }

                yield return new WaitForSeconds(_spawnTimer);

                if (Math.Abs(_spawnTimer - _minSpawnTime) < .01f)
                {
                    _blueEnemyCount++;
                    _redEnemyCount += 4;
                }
            
                _spawnTimer = Mathf.Max(_spawnTimer - 0.5f, _minSpawnTime);
            }
        }

        
        private void SpawnEnemy(EnemyTypes enemyType)
        {
            Enemy enemy = _enemyFactories[enemyType].GetObject();
            enemy.OnItemRelease += ReleaseEnemyFromList;
            
            _spawnersHandler.AddEnemy(enemy);
        
            InitEnemy(enemy,enemyType, GetRandomPosition());
        }

        private void InitEnemy(Enemy enemy, EnemyTypes enemyType, Vector3 startPosition) =>
            enemy.Init(startPosition, _playerTransform, _enemySettings.GetEnemyData(enemyType));

        private void ReleaseEnemyFromList(GameObject enemy) =>
            _spawnersHandler.RemoveEnemy(enemy.GetComponent<Enemy>());

        private Vector3 GetRandomPosition()
        {
            Vector3 resultPoint = Vector3.zero;
            while (resultPoint == Vector3.zero)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * spawnRadius;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas)) {
                    resultPoint = hit.position;
                }
            }
            return resultPoint;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawWireSphere(transform.position, spawnRadius);
        }
    }
}
