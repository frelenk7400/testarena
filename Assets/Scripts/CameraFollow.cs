using Player;
using UnityEngine;
using Zenject;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Vector3 offset;

    private Transform _playerTransform;

    [Inject]
    public void Constrcut(PlayerPresenter playerTransform)
    {
        _playerTransform = playerTransform.transform;
    }

    private void Update()
    {
        transform.position = offset + _playerTransform.position;
        transform.rotation = _playerTransform.rotation;
    }
}
