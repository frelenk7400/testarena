using Infrastructure.Factories;
using Infrastructure.Installers;
using Misc.Bullet;

public class BulletSpawner
{
   private GameSettingsInstaller.BulletSettings _bulletSettings;
   
   private readonly BulletFactory<PlayerBullet> _playerBulletFactory;
   private readonly BulletFactory<EnemyBullet> _enemyBulletFactory;
   
   public BulletSpawner(GameSettingsInstaller.BulletSettings bulletSettings)
   {
      _bulletSettings = bulletSettings;
      
      _playerBulletFactory = new BulletFactory<PlayerBullet>(bulletSettings.PlayerBulletPrefab);
      _enemyBulletFactory = new BulletFactory<EnemyBullet>(bulletSettings.EnemyBulletPrefab);
   }

   public PlayerBullet GetPlayerBullet()
   {
      var bullet = _playerBulletFactory.GetObject();
      bullet.SetSettings(_bulletSettings.PlayerBulletSettings);
      return bullet;
   }

   public EnemyBullet GetEnemyBullet()
   {
      var bullet = _enemyBulletFactory.GetObject();
      bullet.SetSettings(_bulletSettings.EnemyBulletSettings);
      return bullet;
   }

}
