using Infrastructure.Interfaces;
using Spawners;
using UnityEngine;
using UnityEngine.AI;
using Zenject;
using Random = UnityEngine.Random;

namespace Player
{
    public class PlayerPresenter : MonoBehaviour, IDamageableHP, IDamageableUlta
    {
        [SerializeField] private CharacterController characterController;
        [SerializeField] private Transform startShootPosition;
        [SerializeField] private int boundariesLayerMask = 7;

        private PlayerShootHandler shootHandler;
        private PlayerModel _model;
        private PlayerControls _controls;

        private SpawnersHandler _spawnersHandler;
        private BulletSpawner _bulletSpawner;

        public int CurrentHp => _model.CurrentHp;

        [Inject]
        public void Construct(SpawnersHandler spawnersHandler, BulletSpawner bulletSpawner)
        {
            _spawnersHandler = spawnersHandler;
            _bulletSpawner = bulletSpawner;
        }
    
        public void Init(PlayerModel playerModel)
        {
            _model = playerModel;
            _controls = new PlayerControls(characterController, transform,
                _model.MovementSpeed, _model.RotationSpeed);
        
            shootHandler = new PlayerShootHandler(startShootPosition, _bulletSpawner, _spawnersHandler, _model);

            _spawnersHandler.OnEnemyDie += OnEnemyDie;
        }

        public void OnMovementVectorChange(Vector2 movementVector) =>
            _controls.Move(movementVector);

        public void OnRotationVectorChange(Vector2 rotationVector) =>
            _controls.Rotate(rotationVector);

        public void OnAttackButtonPress() =>
            shootHandler.Attack();

        public void OnUltaButtonPress()
        {
            if (_model.CurrentUlta >= 100)
            {
                _spawnersHandler.KillAllEnemies();
                _model.ChangeUltaValue(-100);
            }
        }
        

        public void SetDamage(int damage, bool killedByReflection) =>
            _model.ChangeHp(-damage);

        public void ChangeUltaValue(int ultaValue)
            => _model.ChangeUltaValue(ultaValue);

        public void OnEnemyDie(int ultaValue, int hpValue)
        {
            ChangeUltaValue(ultaValue);
            _model.ChangeHp(hpValue);
        }
    
        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.layer == boundariesLayerMask)
            {
                SetFurthestPositionFromEnemies();
            }
        }

        public Vector3 GetRandomPosition()
        {
            Vector3 resultPoint = Vector3.zero;
            while (resultPoint == Vector3.zero)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * 10;
                NavMeshHit hit;
                if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, NavMesh.AllAreas)) {
                    resultPoint = hit.position;
                }
            }
            return resultPoint;
        }
    
        private void SetFurthestPositionFromEnemies()
        {
            bool positionFound = false;
            Vector3 randomPosition = Vector3.zero;

            while (!positionFound)
            {
                randomPosition = GetRandomPosition();
                positionFound = _spawnersHandler.IsPositionAwayFromEnemies(randomPosition, 2.5f);
            }

            randomPosition.y = transform.position.y;
            transform.position = randomPosition;
        
            _model.OnPlayerRandomTeleportation();
        }
    }
}
