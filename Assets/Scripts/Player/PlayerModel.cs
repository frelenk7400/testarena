using System;
using Infrastructure.Installers;
using UnityEngine;

namespace Player
{
    public class PlayerModel
    {
        public event Action OnPlayerPositionChanged;
        public event Action OnPlayerDie;
        public int CurrentUlta { get; set; }
        public int MaxUlta => _playerSettings.ModelSettings.maxUlta;
    
        public int CurrentHp { get; set; }
        public int MaxHP => _playerSettings.ModelSettings.maxHp;
    
        private readonly PlayerView _playerView;
        public readonly GameSettingsInstaller.PlayerSettings _playerSettings;

        public PlayerModel(PlayerView playerView, GameSettingsInstaller.PlayerSettings playerSettings)
        {
            _playerView = playerView;
            _playerSettings = playerSettings;

            CurrentHp = playerSettings.ModelSettings.startHp;
            CurrentUlta = playerSettings.ModelSettings.startUlta;
        
            _playerView.DisplayUlta(CurrentUlta, MaxUlta);
            _playerView.DisplayHP(CurrentHp);
        }

        public float MovementSpeed => _playerSettings.ModelSettings.movementSpeed;
        public float RotationSpeed => _playerSettings.ModelSettings.angularSpeed;
    
        public void ChangeHp(int damage)
        {
            CurrentHp += damage;
            CurrentHp = Mathf.Clamp(CurrentHp, 0, MaxHP);

            _playerView.DisplayHP(CurrentHp);
        
            if (CurrentHp == 0)
            {
                OnPlayerDie?.Invoke();
                _playerView.DeactivateUI();
            }
        }
    
        public void ChangeUltaValue(int ultaValue)
        {
            CurrentUlta += ultaValue;
            CurrentUlta = Mathf.Clamp(CurrentUlta, 0, MaxUlta);

            _playerView.DisplayUlta(CurrentUlta, MaxUlta);
        }

        public void OnPlayerRandomTeleportation() =>
            OnPlayerPositionChanged?.Invoke();
    
    
        [Serializable]
        public class Settings
        {
            public int maxHp;
            public int startHp;
            public int maxUlta;
            public int startUlta;
            public float movementSpeed;
            public float angularSpeed;
        }
    }
}
