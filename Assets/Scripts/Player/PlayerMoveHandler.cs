using UnityEngine;
using UnityEngine.AI;
using static System.Single;

namespace Player
{
    public class PlayerControls
    {
        private const int radiusToFindNewPosition = 6;
        
        private readonly float _movementSpeed;
        private readonly float _rotationSpeed;
        private readonly CharacterController _characterController;
        private readonly Transform _transform;
        private float _camPitch;

        public PlayerControls(CharacterController characterController, Transform transform,
            float movementSpeed, float rotationSpeed)
        {
            _characterController = characterController;
            _transform = transform;
            _movementSpeed = movementSpeed;
            _rotationSpeed = rotationSpeed;
        }
        
        public void Move(Vector3 movementAxis)
        {
            
            if (movementAxis.sqrMagnitude > Epsilon)
            {
                Vector3 movementVector = _transform.TransformDirection(new Vector3(movementAxis.x, 0, movementAxis.y));
                movementVector.Normalize();
                movementVector *= Time.deltaTime * _movementSpeed;

                _characterController.Move(movementVector);
            }
        }

        public void Rotate(Vector3 rotationAxis)
        {
            if (rotationAxis.sqrMagnitude > Epsilon)
            {
                rotationAxis *= _rotationSpeed * Time.deltaTime;
                
                _camPitch -= rotationAxis.y;
                _camPitch = Mathf.Clamp(_camPitch, -30, 30);
                
                Vector3 rotation = _transform.localEulerAngles;
                rotation.x = _camPitch;
                rotation.y += rotationAxis.x;

                _transform.localEulerAngles = rotation;
            }
        }

        public void MoveToRandomPosition()
        {
          
        }
        
        
    }
}
