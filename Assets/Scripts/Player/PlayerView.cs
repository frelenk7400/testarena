using SimpleInputNamespace;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerView : MonoBehaviour
    {
        [SerializeField] private Button attackButton;
        [SerializeField] private Button ultaButton;
        [SerializeField] private Joystick movementJoystick;
        [SerializeField] private Joystick rotationJoystick;
        [SerializeField] private TextMeshProUGUI healthLabel;
        [SerializeField] private Image ultaBar;
    
        private PlayerPresenter _playerPresenter;

        public void Init(PlayerPresenter playerPresenter)
        {
            _playerPresenter = playerPresenter;
            movementJoystick.OnJoystickValueChange += _playerPresenter.OnMovementVectorChange;
            rotationJoystick.OnJoystickValueChange += _playerPresenter.OnRotationVectorChange;
            attackButton.onClick.AddListener(OnAttackButtonPress);
            ultaButton.onClick.AddListener(OnUltaButtonPress);
        }

        public void DisplayHP(int currentHp) =>
            healthLabel.text = currentHp.ToString();

        public void DisplayUlta(int ulta, int maxUlta)
        {
            ultaBar.fillAmount = ulta/(float)maxUlta;
        }

        private void OnAttackButtonPress() =>
            _playerPresenter.OnAttackButtonPress();
        private void OnUltaButtonPress() =>
            _playerPresenter.OnUltaButtonPress();

        public void DeactivateUI() =>
            gameObject.SetActive(false);
        private void OnDestroy()
        {
            movementJoystick.OnJoystickValueChange -= _playerPresenter.OnMovementVectorChange;
            rotationJoystick.OnJoystickValueChange -= _playerPresenter.OnRotationVectorChange;
        
            attackButton.onClick.RemoveListener(OnAttackButtonPress);
            ultaButton.onClick.RemoveListener(OnUltaButtonPress);
        }
    }
}
