using Misc.Bullet;
using Player;
using Spawners;
using UnityEngine;

public class PlayerShootHandler
{
    private readonly Transform _startShootPosition;
    private readonly int _damage;
    private BulletSpawner _bulletSpawner;
    private SpawnersHandler _spawnersHandler;
    private PlayerModel _playerModel;

    public PlayerShootHandler(Transform startShootPosition, BulletSpawner bulletSpawner,
        SpawnersHandler spawnersHandler, PlayerModel playerModel)
    {
        _startShootPosition = startShootPosition;
        _bulletSpawner = bulletSpawner;
        _spawnersHandler = spawnersHandler;
        _playerModel = playerModel;
    }
    public void Attack()
    {
        PlayerBullet bullet = _bulletSpawner.GetPlayerBullet();
        bullet.Init(_startShootPosition, OnBulletKilledEnemy);
    }

    public void OnBulletKilledEnemy(PlayerBullet playerBullet, Vector3 killPosition)
    {
        Transform nearestEnemyToReflect = _spawnersHandler.GetNearestEnemy(killPosition);
        playerBullet.SetTargetToReflect(nearestEnemyToReflect, _playerModel.CurrentHp);
    }
    
}
