using Infrastructure.Installers;
using StaticVariables;
using UnityEngine;
using UnityEngine.AI;

namespace Enemies
{
   public class AgentMoveToPlayer : MonoBehaviour
   {
      [SerializeField] private EnemyTypes enemyType;
      [SerializeField] private NavMeshAgent agent;

      private float _minimalDistance;
      private Transform _playerTransform;

      public void Init(Transform playerTransform, GameSettingsInstaller.EnemySettings.EnemyData enemySettings)
      {
         _playerTransform = playerTransform;

         RotateTowardsPlayer();
         
         agent.speed = enemySettings.Speed;
         agent.angularSpeed = enemySettings.AngularSpeed;
      }

      private void Update()
      {
         agent.destination = _playerTransform.position;
      }

      private void RotateTowardsPlayer()
      {
         Vector3 lookPosition = _playerTransform.position - transform.position;
         lookPosition.y = 0;
         var rotation = Quaternion.LookRotation(lookPosition);
         transform.rotation = rotation;
      }
   }
}
