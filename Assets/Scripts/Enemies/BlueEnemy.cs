﻿using Infrastructure.Installers;
using Player;
using UnityEngine;
using Zenject;

namespace Enemies
{
    public class BlueEnemy : Enemy
    {
        [SerializeField] private AgentMoveToPlayer agentMoveToPlayer;
        [SerializeField] private EnemyShootHandler enemyShootHandler;
        
        [Inject]
        public void Construct(PlayerModel playerModel)
        {
            playerModel.OnPlayerPositionChanged += enemyShootHandler.OnPlayerTeleportated;
        }
        
        public override void Init(Vector3 startPosition, Transform playerTransform,
            GameSettingsInstaller.EnemySettings.EnemyData enemySettings)
        {
            base.Init(startPosition, playerTransform, enemySettings);
            agentMoveToPlayer.Init(playerTransform, enemySettings);
            enemyShootHandler.Init(playerTransform);
        }
    }
}