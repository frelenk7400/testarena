using System;
using Infrastructure.Installers;
using Misc.Bullet;
using UnityEngine;
using Zenject;

namespace Enemies
{
    public class EnemyShootHandler : MonoBehaviour
    {
        [SerializeField] private Transform startShootPosition;
    
        private const float FireInterval = 3;
    
        private GameSettingsInstaller.EnemySettings.EnemyData _enemyData;
        private Transform _playerTransform;
        private bool _isInit;
        private float _shootTimer;
        private BulletSpawner _bulletSpawner;
        private bool _playerTeleportated;

        private Action OnPlayerPositionChanged;

        [Inject]
        public void Construct(BulletSpawner bulletSpawner)
        {
            _bulletSpawner = bulletSpawner;
        }
    
        public void Init(Transform playerTransform)
        {
            _playerTeleportated = false;
            _playerTransform = playerTransform;
        }

        private void Update()
        {
            _shootTimer += Time.deltaTime;
            if (_shootTimer >= FireInterval)
            {
                EnemyBullet enemyBullet = _bulletSpawner.GetEnemyBullet();
                OnPlayerPositionChanged += Test;
                enemyBullet.Init(startShootPosition, _playerTransform,ref OnPlayerPositionChanged);
        
                _shootTimer = 0;
            }
        }

        public void OnPlayerTeleportated()
        {
            OnPlayerPositionChanged?.Invoke();
        }

        public void Test()
        {
        
        }

    }
}
