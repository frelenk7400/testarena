using System;
using Infrastructure.Installers;
using Infrastructure.Interfaces;
using Misc;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemies
{
    public abstract class Enemy : MonoBehaviour, IDamageableHP, IReleasable
    {
        public int CurrentHp { get; private set; }

        public event Action<int, int> OnEnemyDie; 
        public event Action<GameObject> OnItemRelease;

        protected GameSettingsInstaller.EnemySettings.EnemyData _enemySettings;

        public virtual void Init(Vector3 startPosition, Transform playerTransform,
            GameSettingsInstaller.EnemySettings.EnemyData enemySettings)
        {
            transform.position = startPosition;
            _enemySettings = enemySettings;
            CurrentHp = enemySettings.StartHP;
        }

        public void SetDamage(int damage, bool killedByReflect)
        {
            CurrentHp -= damage;
            if (CurrentHp <= 0)
            {
                int ultaValue = 0;
                int hpValue = 0;

                if (killedByReflect)
                {
                    int chance = Random.Range(0, 100);

                    ultaValue = chance < 50 ? 25 : 0;
                    hpValue = chance > 50 ? 50 : 0;
                }
                else
                {
                    ultaValue = _enemySettings.UltaPointsForDefeat;
                }

                OnEnemyDie?.Invoke(ultaValue, hpValue);
                Release();
            }
        }
        
        public void KillWithoutReward() => Release();

        protected void Release() =>
            OnItemRelease?.Invoke(gameObject);

    }
}
