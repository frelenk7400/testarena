﻿using Infrastructure.Installers;
using Infrastructure.Interfaces;
using UnityEngine;

namespace Enemies
{
    public class RedEnemy : Enemy
    {
        [SerializeField] private AgentKamikaze agentKamikaze;

        public override void Init(Vector3 startPosition, Transform playerTransform,
            GameSettingsInstaller.EnemySettings.EnemyData enemySettings)
        {
            base.Init(startPosition, playerTransform, enemySettings);
            agentKamikaze.Init(playerTransform, enemySettings.Speed);
            agentKamikaze.OnDestinationArrived += AgentArrivedDestination;
        }

        private void AgentArrivedDestination()
        {
            agentKamikaze.OnDestinationArrived -= AgentArrivedDestination;
            Release();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out IDamageableHP hp))
            {
                hp.SetDamage(_enemySettings.Damage);
            }
           
            Release();
        }
    }
}