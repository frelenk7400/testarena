using System;
using System.Collections;
using UnityEngine;

namespace Enemies
{
   public class AgentKamikaze : MonoBehaviour
   {
      public event Action OnDestinationArrived;
   
      private const float UprisingTime = .5f;
      private const float FreezeTime = .5f;
   
      private Transform _playerTransform;
      private Vector3 _oldPlayerPosition;
      private float _speed;

      private bool isAttackInit;

      public void Init(Transform playerTransform, float speed)
      {
         _playerTransform = playerTransform;
         _speed = speed;

         StartCoroutine(PreparationForAttack());
      }

      private void Update()
      {
         if (isAttackInit)
         {
            transform.LookAt(_playerTransform);
            Vector3 directionToPlayer = _oldPlayerPosition - transform.position;
            directionToPlayer.Normalize();
            transform.position += directionToPlayer * _speed * Time.deltaTime;

            if (Vector3.Distance(_oldPlayerPosition, transform.position) < .01f)
            {
               OnDestinationArrived?.Invoke();
            }
         }
      }

      private IEnumerator PreparationForAttack()
      {
         float elapsedTime = 0;
         Vector3 startAgentPosition = transform.position;
         Vector3 endAgentPosition = startAgentPosition + Vector3.up * 2;
      
         while (elapsedTime <= UprisingTime)
         {
            elapsedTime += Time.deltaTime;

            transform.position = Vector3.Slerp(startAgentPosition, endAgentPosition, elapsedTime / UprisingTime);
            yield return null;
         }

         yield return new WaitForSeconds(FreezeTime);

         _oldPlayerPosition = _playerTransform.position;
         isAttackInit = true;
      }
   }
}
