using Player;
using Spawners;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

public class MainMenu : MonoBehaviour
{
   [SerializeField] private Button pauseButton;
   [SerializeField] private Button restartGame;
   [SerializeField] private TextMeshProUGUI enemyKilled;
   [SerializeField] private GameObject gameOverPanel;

   private SpawnersHandler _spawnersHandler;
   
   [Inject]
   public void Constrcut(PlayerModel playerModel, SpawnersHandler spawnersHandler)
   {
      playerModel.OnPlayerDie += OnPlayerDie;
      _spawnersHandler = spawnersHandler;
   }
   
   private void Start()
   {
      pauseButton.onClick.AddListener((() => { Time.timeScale = 0;}));
      restartGame.onClick.AddListener((() => { SceneManager.LoadScene(SceneManager.GetActiveScene().name);}));
   }

   private void OnPlayerDie()
   {
      gameOverPanel.gameObject.SetActive(true);
      enemyKilled.text = $"Enemies killed: {_spawnersHandler.EnemiesKilled}";
   }
}
